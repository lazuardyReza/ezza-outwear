package com.omidev.ezzaoutwear.utils

class Constant {
    object VALUE{
        val ALL = "Semua"
        val LENGAN_PANJANG = "Lengan Panjang"
        val LENGAN_PENDEK  = "Lengan Pendek"
        val BAJU_KOKO = "Baju Koko"
        val MUKENA = "Mukena"
    }
}