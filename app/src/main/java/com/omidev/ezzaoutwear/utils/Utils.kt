package com.omidev.ezzaoutwear.utils

object Utils {
    fun getShownNominalNoneConvert(s: String): String {
        var s = s
        var result = ""
        val a = s.replace(" ", "")
        var j = ""
        var k = a
        if (a.endsWith(".000000")) {
            j = a.substring(0, a.length - 7)
            k = j
        }
        if (a.endsWith(".00")) {
            j = a.substring(0, a.length - 3)
            k = j
        }
        if (a.endsWith(".0")) {
            j = a.substring(0, a.length - 2)
            k = j
        }
        val z = k.replace("-", "")
        val b = z.replace("Rp.", "")
        val c = b.replace("Rp", "")
        val d = c.replace(",00", "")
        s = d.replace(".", "")
        var count = 1
        for (i in s.length - 1 downTo 0) {
            result = s[i].toString() + result
            if (count % 3 == 0 && i != 0) result = ".$result"
            count++
        }
        return "Rp $result"
    }
}