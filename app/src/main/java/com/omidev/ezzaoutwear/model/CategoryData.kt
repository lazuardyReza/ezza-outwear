package com.omidev.ezzaoutwear.model

import com.omidev.ezzaoutwear.R
import com.omidev.ezzaoutwear.utils.Constant

object CategoryData {
    private val categoryName = arrayOf(Constant.VALUE.ALL, Constant.VALUE.LENGAN_PANJANG, Constant.VALUE.LENGAN_PENDEK, Constant.VALUE.BAJU_KOKO, Constant.VALUE.MUKENA)

    private val categoryImage = intArrayOf(
        R.drawable.ic_all,
        R.drawable.ic_longshirt,
        R.drawable.ic_shirt,
        R.drawable.ic_muslim,
        R.drawable.ic_muslim_woman)

    val listData: ArrayList<Category>
        get() {
            val list = arrayListOf<Category>()
            for (position in categoryName.indices) {
                val category = Category()
                category.name = categoryName[position]
                category.image = categoryImage[position]
                list.add(category)
            }
            return list
        }
}