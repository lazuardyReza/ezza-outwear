package com.omidev.ezzaoutwear.model

data class Category (
    var name: String = "",
    var image: Int = 0
)