package com.omidev.ezzaoutwear.model

data class Product (
    var name: String = "",
    var description: String = "",
    var photo: Int = 0,
    var price: Int = 0,
    var stock: Int = 0,
    var categories: String = ""
)