package com.omidev.ezzaoutwear.model

import com.omidev.ezzaoutwear.R
import com.omidev.ezzaoutwear.utils.Constant

object ProductData {
    private val productName = arrayOf(
        "GL003","GL004","GL005","GL006","GL007","GL009","GL013","GL015","GL023"
        ,"GS004","GS006","GS010","GS011","GS013","GS014","GS015","GS020",
        "Kurta 1","Kurta 2", "Kurta 3", "Kurta 4","Kurta 5","Kurta 6","Kurta 7",
        "Kurta Anak Toyobo 1","Kurta Anak Toyobo 2", "Kurta Anak Toyobo 3", "Kurta Anak Toyobo 4", "Kurta Anak Toyobo 5",
        "Kurta Anak Kenkids 1","Kurta Anak Kenkids 2", "Kurta Anak Kenkids 3", "Kurta Anak Kenkids 4",
        "Mukena 1", "Mukena 2", "Mukena 3","Mukena 4", "Mukena 5","Mukena 6", "Mukena 7", "Mukena 8", "Mukena 9","Mukena 10")

    private val productImage = intArrayOf(
        R.drawable.gl003,R.drawable.gl004,R.drawable.gl005, R.drawable.gl006, R.drawable.gl007,R.drawable.gl009,
        R.drawable.gl013, R.drawable.gl015, R.drawable.gl023, R.drawable.gs004, R.drawable.gs006, R.drawable.gs10,
        R.drawable.gs11, R.drawable.gs013, R.drawable.gs014, R.drawable.gs015, R.drawable.gs020, R.drawable.kurta_1,
        R.drawable.kurta_2, R.drawable.kurta_3, R.drawable.kurta_4, R.drawable.kurta_5, R.drawable.kurta_6, R.drawable.kurta_7,
        R.drawable.kurta_anak_katun_toyobo_1, R.drawable.kurta_anak_katun_toyobo_2, R.drawable.kurta_anak_katun_toyobo_3,
        R.drawable.kurta_anak_katun_toyobo_4, R.drawable.kurta_anak_katun_toyobo_5, R.drawable.kurta_anak_kenkids_1,
        R.drawable.kurta_anak_kenkids_2, R.drawable.kurta_anak_kenkids_3, R.drawable.kurta_anak_kenkids_4, R.drawable.mukena_katun_1,
        R.drawable.mukena_katun_2, R.drawable.mukena_katun_3, R.drawable.mukena_katun_4, R.drawable.mukena_katun_5, R.drawable.mukena_katun_6,
        R.drawable.mukena_katun_7, R.drawable.mukena_katun_8, R.drawable.mukena_katun_9, R.drawable.mukena_katun_10)

    private val productDescription = arrayOf(
        "[Produk Lengan Panjang GL003]\n" +
                "\n" +
                "Rp 110.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 200.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Panjang GL004]\n" +
                "\n" +
                "Rp 110.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 200.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Panjang GL005]\n" +
                "\n" +
                "Rp 110.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 200.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Panjang GL006]\n" +
                "\n" +
                "Rp 110.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 200.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Panjang GL007]\n" +
                "\n" +
                "Rp 110.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 200.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Panjang GL009]\n" +
                "\n" +
                "Rp 110.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 200.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Panjang GL013]\n" +
                "\n" +
                "Rp 110.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 200.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Panjang GL015]\n" +
                "\n" +
                "Rp 110.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 200.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Panjang GL023]\n" +
                "\n" +
                "Rp 110.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 200.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Pendek GS004]\n" +
                "\n" +
                "Rp 100.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 190.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74 cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Pendek GS006]\n" +
                "\n" +
                "Rp 100.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 190.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74 cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Pendek GS010]\n" +
                "\n" +
                "Rp 100.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 190.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74 cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Pendek GL011]\n" +
                "\n" +
                "Rp 100.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 190.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74 cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Pendek GS013]\n" +
                "\n" +
                "Rp 100.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 190.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74 cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Pendek GS014]\n" +
                "\n" +
                "Rp 100.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 190.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74 cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Pendek GS015]\n" +
                "\n" +
                "Rp 100.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 190.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74 cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Lengan Pendek GS020]\n" +
                "\n" +
                "Rp 100.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 190.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "M: (Lebar 50 cm x Panjang 70 cm)\n" +
                "L: (Lebar 52 cm x Panjang 72 cm)\n" +
                "XL: (Lebar 54 cm x Panjang 74 cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Kurta/Muslim K001]\n" +
                "\n" +
                "Rp 125.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 220.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "S: (Lebar 54 cm x Panjang 84 cm)\n" +
                "M: (Lebar 56 cm x Panjang 86 cm)\n" +
                "L: (Lebar 58 cm x Panjang 88 cm)\n" +
                "XL: (Lebar 60 cm x Panjang 90cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Kurta/Muslim K002]\n" +
                "\n" +
                "Rp 125.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 220.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "S: (Lebar 54 cm x Panjang 84 cm)\n" +
                "M: (Lebar 56 cm x Panjang 86 cm)\n" +
                "L: (Lebar 58 cm x Panjang 88 cm)\n" +
                "XL: (Lebar 60 cm x Panjang 90cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Kurta/Muslim K003]\n" +
                "\n" +
                "Rp 125.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 220.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "S: (Lebar 54 cm x Panjang 84 cm)\n" +
                "M: (Lebar 56 cm x Panjang 86 cm)\n" +
                "L: (Lebar 58 cm x Panjang 88 cm)\n" +
                "XL: (Lebar 60 cm x Panjang 90cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Kurta/Muslim K004]\n" +
                "\n" +
                "Rp 125.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 220.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "S: (Lebar 54 cm x Panjang 84 cm)\n" +
                "M: (Lebar 56 cm x Panjang 86 cm)\n" +
                "L: (Lebar 58 cm x Panjang 88 cm)\n" +
                "XL: (Lebar 60 cm x Panjang 90cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Kurta/Muslim K005]\n" +
                "\n" +
                "Rp 125.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 220.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "S: (Lebar 54 cm x Panjang 84 cm)\n" +
                "M: (Lebar 56 cm x Panjang 86 cm)\n" +
                "L: (Lebar 58 cm x Panjang 88 cm)\n" +
                "XL: (Lebar 60 cm x Panjang 90cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Kurta/Muslim K006]\n" +
                "\n" +
                "Rp 125.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 220.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "S: (Lebar 54 cm x Panjang 84 cm)\n" +
                "M: (Lebar 56 cm x Panjang 86 cm)\n" +
                "L: (Lebar 58 cm x Panjang 88 cm)\n" +
                "XL: (Lebar 60 cm x Panjang 90cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Produk Kurta/Muslim K007]\n" +
                "\n" +
                "Rp 125.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 220.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "S: (Lebar 54 cm x Panjang 84 cm)\n" +
                "M: (Lebar 56 cm x Panjang 86 cm)\n" +
                "L: (Lebar 58 cm x Panjang 88 cm)\n" +
                "XL: (Lebar 60 cm x Panjang 90cm)\n" +
                "\n" +
                "Note: Untuk ukuran Lingkar Dada = Lebar Dada\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Muslim Kids Outwear 1]\n" +
                "\n" +
                "Rp 75.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 130.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "Ukuran 6\n" +
                "Pb: 63cm\n" +
                "Ld: 37cm\n" +
                "\n" +
                "Ukuran 8\n" +
                "Pb: 63cm\n" +
                "Ld: 39cm\n" +
                "\n" +
                "Ukuran 6\n" +
                "Pb: 65cm\n" +
                "Ld: 41cm\n" +
                "\n" +
                "Bahan: Katun Toyobo\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Muslim Kids Outwear 2]\n" +
                "\n" +
                "Rp 75.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 130.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "Ukuran 6\n" +
                "Pb: 63cm\n" +
                "Ld: 37cm\n" +
                "\n" +
                "Ukuran 8\n" +
                "Pb: 63cm\n" +
                "Ld: 39cm\n" +
                "\n" +
                "Ukuran 6\n" +
                "Pb: 65cm\n" +
                "Ld: 41cm\n" +
                "\n" +
                "Bahan: Katun Toyobo\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Muslim Kids Outwear 3]\n" +
                "\n" +
                "Rp 75.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 130.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "Ukuran 6\n" +
                "Pb: 63cm\n" +
                "Ld: 37cm\n" +
                "\n" +
                "Ukuran 8\n" +
                "Pb: 63cm\n" +
                "Ld: 39cm\n" +
                "\n" +
                "Ukuran 6\n" +
                "Pb: 65cm\n" +
                "Ld: 41cm\n" +
                "\n" +
                "Bahan: Katun Toyobo\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Muslim Kids Outwear 4]\n" +
                "\n" +
                "Rp 75.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 130.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "Ukuran 6\n" +
                "Pb: 63cm\n" +
                "Ld: 37cm\n" +
                "\n" +
                "Ukuran 8\n" +
                "Pb: 63cm\n" +
                "Ld: 39cm\n" +
                "\n" +
                "Ukuran 6\n" +
                "Pb: 65cm\n" +
                "Ld: 41cm\n" +
                "\n" +
                "Bahan: Katun Toyobo\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",

        "[Muslim Kids Outwear 5]\n" +
                "\n" +
                "Rp 75.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 130.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "Ukuran 6\n" +
                "Pb: 63cm\n" +
                "Ld: 37cm\n" +
                "\n" +
                "Ukuran 8\n" +
                "Pb: 63cm\n" +
                "Ld: 39cm\n" +
                "\n" +
                "Ukuran 6\n" +
                "Pb: 65cm\n" +
                "Ld: 41cm\n" +
                "\n" +
                "Bahan: Katun Toyobo\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Koko Kurta Kenz 1]\n" +
                "\n" +
                "Rp 75.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 130.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "Ukuran S\n" +
                "Ld: 60cm\n" +
                "Pb: 40cm\n" +
                "Pc: 42cm\n" +
                "\n" +
                "Ukuran M\n" +
                "Ld: 74cm\n" +
                "Pb: 52cm\n" +
                "Pc: 55cm\n" +
                "\n" +
                "Ukuran L\n" +
                "Ld: 76cm\n" +
                "Pb: 55cm\n" +
                "Pc: 65cm\n" +
                "\n" +
                "Ukuran XL\n" +
                "Ld: 78cm\n" +
                "Pb: 60cm\n" +
                "Pc: 70cm\n" +
                "\n" +
                "Bahan: Katun Toyobo\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Koko Kurta Kenz 2]\n" +
                "\n" +
                "Rp 75.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 130.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "Ukuran S\n" +
                "Ld: 60cm\n" +
                "Pb: 40cm\n" +
                "Pc: 42cm\n" +
                "\n" +
                "Ukuran M\n" +
                "Ld: 74cm\n" +
                "Pb: 52cm\n" +
                "Pc: 55cm\n" +
                "\n" +
                "Ukuran L\n" +
                "Ld: 76cm\n" +
                "Pb: 55cm\n" +
                "Pc: 65cm\n" +
                "\n" +
                "Ukuran XL\n" +
                "Ld: 78cm\n" +
                "Pb: 60cm\n" +
                "Pc: 70cm\n" +
                "\n" +
                "Bahan: Katun Toyobo\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Koko Kurta Kenz 3]\n" +
                "\n" +
                "Rp 75.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 130.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "Ukuran S\n" +
                "Ld: 60cm\n" +
                "Pb: 40cm\n" +
                "Pc: 42cm\n" +
                "\n" +
                "Ukuran M\n" +
                "Ld: 74cm\n" +
                "Pb: 52cm\n" +
                "Pc: 55cm\n" +
                "\n" +
                "Ukuran L\n" +
                "Ld: 76cm\n" +
                "Pb: 55cm\n" +
                "Pc: 65cm\n" +
                "\n" +
                "Ukuran XL\n" +
                "Ld: 78cm\n" +
                "Pb: 60cm\n" +
                "Pc: 70cm\n" +
                "\n" +
                "Bahan: Katun Toyobo\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "[Koko Kurta Kenz 4]\n" +
                "\n" +
                "Rp 75.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 130.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Estimasi Ukuran :\n" +
                "Ukuran S\n" +
                "Ld: 60cm\n" +
                "Pb: 40cm\n" +
                "Pc: 42cm\n" +
                "\n" +
                "Ukuran M\n" +
                "Ld: 74cm\n" +
                "Pb: 52cm\n" +
                "Pc: 55cm\n" +
                "\n" +
                "Ukuran L\n" +
                "Ld: 76cm\n" +
                "Pb: 55cm\n" +
                "Pc: 65cm\n" +
                "\n" +
                "Ukuran XL\n" +
                "Ld: 78cm\n" +
                "Pb: 60cm\n" +
                "Pc: 70cm\n" +
                "\n" +
                "Bahan: Katun Toyobo\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n" +
                "FORMAT ORDER :\n" +
                "* Kode kemeja dan ukurannya\n" +
                "* Nama lengkap dan no hp aktif\n" +
                "* Alamat lengkap (sertakan RT/RW)\n" +
                "* Kurir pilihan\n" +
                "\n" +
                "Contoh:\n" +
                "S228 L\n" +
                "Ade, 0812345678\n" +
                "Jl menuju sukses, No.12, RT2/RW6, kecamatan bahagia, kabupaten syukur, kota maju, kode pos 123456\n" +
                "JNE REG  (pilih salah satu)\n" +
                "\n" +
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "Mukena Katun Silky 1\n" +
                "\n" +
                "Rp 185.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 360.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Bahan: Katun Silky\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n"+
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "Mukena Katun Silky 2\n" +
                "\n" +
                "Rp 185.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 360.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Bahan: Katun Silky\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n"+
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "Mukena Katun Silky 3\n" +
                "\n" +
                "Rp 185.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 360.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Bahan: Katun Silky\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n"+
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "Mukena Katun Silky 4\n" +
                "\n" +
                "Rp 185.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 360.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Bahan: Katun Silky\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n"+
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "Mukena Katun Silky 5\n" +
                "\n" +
                "Rp 185.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 360.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Bahan: Katun Silky\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n"+
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "Mukena Katun Silky 6\n" +
                "\n" +
                "Rp 185.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 360.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Bahan: Katun Silky\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n"+
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "Mukena Katun Silky 7\n" +
                "\n" +
                "Rp 185.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 360.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Bahan: Katun Silky\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n"+
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "Mukena Katun Silky 8\n" +
                "\n" +
                "Rp 185.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 360.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Bahan: Katun Silky\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n"+
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "Mukena Katun Silky 9\n" +
                "\n" +
                "Rp 185.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 360.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Bahan: Katun Silky\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n"+
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)",
        "Mukena Katun Silky 10\n" +
                "\n" +
                "Rp 185.000 \n" +
                "BELI 2 DENGAN TIPE KODE YANG SAMA HANYA Rp. 360.000\n" +
                "(tidak termasuk ongkos kirim) \n" +
                "\n" +
                "READY STOK !!! \n" +
                "Dipersilahkan untuk teman-teman yang ingin order baca dahulu sebelum melakukan order yaaah... \n" +
                "\n" +
                "Bahan: Katun Silky\n" +
                "\n" +
                "PERHATIAN :\n" +
                "* Harga yang tercantum BELUM TERMASUK ongkos kirim\n" +
                "* Promo berlaku untuk produk dengan tipe kode (kode awalan) yang sama, misal beli 2 produk K002 dengan K007\n" +
                "* Promo tidak berlaku untuk produk dengan kode berbeda\n" +
                "* Silahkan tanya dulu model dan size yang teman-teman inginkan sebelum order untuk menghindari kekosongan stock dan ukuran\n" +
                "* ADANYA KEMUNGKINAN PERBEDAAN SEDIKIT WARNA KARENA EFEK CAHAYA KAMERA\n" +
                "* Produk kami adalah ORIGINAL dengan kualitas TERBAIK\n" +
                "* TIDAK MELAYANI PERUBAHAN WARNA/SIZE\n" +
                "* Silahkan teman-teman hubungi nomor yang ada di \"Kontak\" untuk proses lebih lanjut\n" +
                "\n"+
                "TERIMA KASIH\n" +
                "HAPPY SHOPPING :)"

    )

    private val productStock = intArrayOf(3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3)

    private val productPrice = intArrayOf(110000,110000,110000,110000,110000,110000,110000,110000,110000,100000,100000,100000,100000,100000,100000,100000,100000,125000,125000,125000,125000,125000,
        125000,125000,75000,75000,75000,75000,75000,75000,75000,75000,75000,185000,185000,185000,185000,185000,185000,185000,185000,185000,185000)

    private val productCategories = arrayOf(Constant.VALUE.LENGAN_PANJANG,Constant.VALUE.LENGAN_PANJANG,Constant.VALUE.LENGAN_PANJANG,
        Constant.VALUE.LENGAN_PANJANG,Constant.VALUE.LENGAN_PANJANG,Constant.VALUE.LENGAN_PANJANG,Constant.VALUE.LENGAN_PANJANG,Constant.VALUE.LENGAN_PANJANG,Constant.VALUE.LENGAN_PANJANG,
        Constant.VALUE.LENGAN_PENDEK,Constant.VALUE.LENGAN_PENDEK,Constant.VALUE.LENGAN_PENDEK,Constant.VALUE.LENGAN_PENDEK,Constant.VALUE.LENGAN_PENDEK,
        Constant.VALUE.LENGAN_PENDEK,Constant.VALUE.LENGAN_PENDEK,Constant.VALUE.LENGAN_PENDEK, Constant.VALUE.BAJU_KOKO, Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,
        Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,
        Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,Constant.VALUE.BAJU_KOKO,
        Constant.VALUE.MUKENA,Constant.VALUE.MUKENA,Constant.VALUE.MUKENA,Constant.VALUE.MUKENA,Constant.VALUE.MUKENA,Constant.VALUE.MUKENA,Constant.VALUE.MUKENA,
        Constant.VALUE.MUKENA,Constant.VALUE.MUKENA,Constant.VALUE.MUKENA)

    val listData: ArrayList<Product>
        get() {
            val list = arrayListOf<Product>()
            for (position in productName.indices) {
                val product = Product()
                product.name = productName[position]
                product.photo = productImage[position]
                product.description = productDescription[position]
                product.stock = productStock[position]
                product.price = productPrice[position]
                product.categories = productCategories[position]
                list.add(product)
            }
            return list
        }

//    val listLenganPanjang: ArrayList<Product>
//        get() {
//            val list = arrayListOf<Product>()
//            for (position in productName.indices) {
//                val product = Product()
//                product.name = productName[position]
//                product.photo = productImage[position]
//                product.description = productDescription[position]
//                product.stock = productStock[position]
//                product.price = productPrice[position]
//                product.categories = productCategories[position]
//                list.add(product)
//            }
//            list.filter {  }
//            return list
//        }
}
