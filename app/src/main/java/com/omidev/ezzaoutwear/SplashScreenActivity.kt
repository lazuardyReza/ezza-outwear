package com.omidev.ezzaoutwear

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.Window

class SplashScreenActivity : AppCompatActivity() {
    private var countDownTimer: CountDownTimer? = null
    private var isShowRootAlert: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        initTimerSplashScreen()
    }

    private fun initTimerSplashScreen() {
        countDownTimer = object : CountDownTimer(3000, 1000) {
            override fun onFinish() {
                if (!isFinishing) {
                    intent = Intent(applicationContext,MainActivity::class.java)
                    startActivity(intent)
                    // close splash activity
                    finish()
                }
            }
            override fun onTick(millisUntilFinished: Long) {}
        }
        countDownTimer?.start()
    }



    override fun onResume() {
        super.onResume()
        initTimerSplashScreen()
    }

    override fun onPause() {
        super.onPause()
        countDownTimer?.cancel()
    }
}
