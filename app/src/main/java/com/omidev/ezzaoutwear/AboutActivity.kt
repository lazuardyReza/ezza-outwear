package com.omidev.ezzaoutwear

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_about.*
import kotlinx.android.synthetic.main.toolbar.*


class AboutActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        initComponent()
    }
    fun initComponent() {
        setSupportActionBar(toolbar)
        lblToolbar.text = title
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        toolbar.navigationIcon = null
        lblToolbar.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        ivleft.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.nav_back))
        ivleft.setOnClickListener(this)
        ivAbout.visibility = View.GONE

        tv_omidev.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v){
            ivleft->{
                finish()
            }
            tv_omidev ->{
                val uri: Uri =
                    Uri.parse("http://www.omidev.com")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            }
        }
    }
}
