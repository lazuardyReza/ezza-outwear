package com.omidev.ezzaoutwear

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.omidev.ezzaoutwear.adapter.CardViewProductAdapter
import com.omidev.ezzaoutwear.adapter.GridProductAdapter
import com.omidev.ezzaoutwear.adapter.ListCategoryAdapter
import com.omidev.ezzaoutwear.model.Category
import com.omidev.ezzaoutwear.model.CategoryData
import com.omidev.ezzaoutwear.model.Product
import com.omidev.ezzaoutwear.model.ProductData
import com.omidev.ezzaoutwear.utils.Constant
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var rvCategory: RecyclerView
    private lateinit var  cardViewProductAdapter: CardViewProductAdapter
    private var listCategory: ArrayList<Category> = arrayListOf()
    private var listProduct: ArrayList<Product> = arrayListOf()
    private var listProductFilter: ArrayList<Product> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initComponent()
    }

    fun initComponent(){
        setSupportActionBar(toolbar)
        lblToolbar.text = title
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        toolbar.navigationIcon = null
        lblToolbar.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        ivAbout.setOnClickListener(this)

        rvCategory = findViewById(R.id.rvCategory)
        rvCategory.setHasFixedSize(true)

        listCategory.addAll(CategoryData.listData)
        showCategoryRecycler()

        listProduct.addAll(ProductData.listData)
        showProductRecycler()
    }

    fun showCategoryRecycler(){
        rvCategory.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val listCategoryAdapter =ListCategoryAdapter(listCategory)
        rvCategory.adapter = listCategoryAdapter

        listCategoryAdapter.setOnItemClickCallback(object: ListCategoryAdapter.OnItemClickCallback{
            override fun onItemClicked(data: Category) {
                showSelectedCategory(data)
            }
        })
    }

    fun showProductRecycler(){
        rvProduct.layoutManager = GridLayoutManager(this, 2)
        cardViewProductAdapter = CardViewProductAdapter(this,listProduct)
        rvProduct.adapter = cardViewProductAdapter
    }

    fun showSelectedCategory(data:Category){
        when(data.name){
            Constant.VALUE.ALL ->{
                tvProduk.text = Constant.VALUE.ALL
                listProductFilter.clear()
                listProductFilter.addAll(listProduct)
                cardViewProductAdapter?.setItems(listProductFilter)
                cardViewProductAdapter?.notifyDataSetChanged()
            }
            Constant.VALUE.LENGAN_PANJANG ->{
                tvProduk.text = Constant.VALUE.LENGAN_PANJANG
                listProductFilter.clear()
                listProductFilter.addAll(listProduct.filter { it.categories == Constant.VALUE.LENGAN_PANJANG })
                cardViewProductAdapter?.setItems(listProductFilter)
                cardViewProductAdapter?.notifyDataSetChanged()
            }
            Constant.VALUE.LENGAN_PENDEK ->{
                tvProduk.text = Constant.VALUE.LENGAN_PENDEK
                listProductFilter.clear()
                listProductFilter.addAll(listProduct.filter { it.categories == Constant.VALUE.LENGAN_PENDEK })
                cardViewProductAdapter?.setItems(listProductFilter)
                cardViewProductAdapter?.notifyDataSetChanged()
            }
            Constant.VALUE.BAJU_KOKO ->{
                tvProduk.text = Constant.VALUE.BAJU_KOKO
                listProductFilter.clear()
                listProductFilter.addAll(listProduct.filter { it.categories == Constant.VALUE.BAJU_KOKO })
                cardViewProductAdapter?.setItems(listProductFilter)
                cardViewProductAdapter?.notifyDataSetChanged()
            }
            Constant.VALUE.MUKENA ->{
                tvProduk.text = Constant.VALUE.MUKENA
                listProductFilter.clear()
                listProductFilter.addAll(listProduct.filter { it.categories == Constant.VALUE.MUKENA })
                cardViewProductAdapter?.setItems(listProductFilter)
                cardViewProductAdapter?.notifyDataSetChanged()
            }
        }
    }

    override fun onClick(v: View?) {
        when(v){
            ivAbout->{
                val productIntent = Intent(this, AboutActivity::class.java)
                this.overridePendingTransition(R.anim.exit_to_right,
                    R.anim.enter_from_left);
                this.startActivity(productIntent)
            }
        }
    }
}
