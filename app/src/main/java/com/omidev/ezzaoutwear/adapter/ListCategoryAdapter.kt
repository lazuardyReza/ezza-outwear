package com.omidev.ezzaoutwear.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.omidev.ezzaoutwear.R
import com.omidev.ezzaoutwear.model.Category

class ListCategoryAdapter(private val listCategory:ArrayList<Category>): RecyclerView.Adapter<ListCategoryAdapter.ListViewHolder>() {
    private lateinit var onItemClickCallback: OnItemClickCallback

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ListViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_category, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listCategory.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val category = listCategory[position]
        Glide.with(holder.itemView.context)
            .load(category.image)
            .apply(RequestOptions().override(55, 55))
            .into(holder.imgCategory)
        holder.tvCategory.text = category.name
        holder.itemView.setOnClickListener{
            onItemClickCallback.onItemClicked(listCategory[holder.adapterPosition])
        }
        holder.imgCategory.setOnClickListener{
            onItemClickCallback.onItemClicked(listCategory[holder.adapterPosition])
        }
    }

    inner class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var tvCategory: TextView = itemView.findViewById(R.id.tvCategory)
        var imgCategory: ImageView = itemView.findViewById(R.id.btn_category)
    }

    interface OnItemClickCallback{
        fun onItemClicked(data:Category)
    }

}