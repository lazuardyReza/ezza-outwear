package com.omidev.ezzaoutwear.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.omidev.ezzaoutwear.ProductDetailActivity
import com.omidev.ezzaoutwear.R
import com.omidev.ezzaoutwear.model.Product
import com.omidev.ezzaoutwear.utils.Utils

class CardViewProductAdapter(var context: Context, private var listProduct: ArrayList<Product> ): RecyclerView.Adapter<CardViewProductAdapter.CardViewHeroHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHeroHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.item_cardview_product, parent, false)
        return CardViewHeroHolder(view)
    }

    override fun getItemCount(): Int {
       return listProduct.size
    }

    override fun onBindViewHolder(holder: CardViewHeroHolder, position: Int) {
       val product = listProduct[position]

        Glide.with(holder.itemView.context)
            .load(product.photo)
            .apply(RequestOptions().override(350,550))
            .into(holder.imgPhoto)

        holder.tvName.text = product.name
        val description = product.description.toString().substring(0,28)
        holder.tvDetail.text = description+"..."
        holder.tvPrice.text = Utils.getShownNominalNoneConvert(product.price.toString())
        holder.itemView.setOnClickListener {
            var selectedProduct = listProduct[holder.adapterPosition]
            val productIntent = Intent(context as Activity, ProductDetailActivity::class.java)
            productIntent.putExtra(ProductDetailActivity.EXTRA_NAME,selectedProduct.name)
            productIntent.putExtra(ProductDetailActivity.EXTRA_PHOTO, selectedProduct.photo)
            productIntent.putExtra(ProductDetailActivity.EXTRA_DESCRIPTION,selectedProduct.description)
            productIntent.putExtra(ProductDetailActivity.EXTRA_STOCK, selectedProduct.stock)
            productIntent.putExtra(ProductDetailActivity.EXTRA_PRICE,selectedProduct.price)
            productIntent.putExtra(ProductDetailActivity.EXTRA_CATEGORIES, selectedProduct.categories)

            (context as Activity).overridePendingTransition(R.anim.exit_to_right,
                R.anim.enter_from_left);
            (context as Activity).startActivity(productIntent)
        }

    }

    inner class CardViewHeroHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgPhoto: ImageView = itemView.findViewById(R.id.img_item_photo)
        var tvName : TextView = itemView.findViewById(R.id.tv_item_name)
        var tvDetail: TextView = itemView.findViewById(R.id.tv_item_detail)
        var tvPrice: TextView = itemView.findViewById(R.id.tv_label_price)
    }

    fun setItems(data: ArrayList<Product>) {
        this.listProduct = data
        notifyDataSetChanged()
    }
}