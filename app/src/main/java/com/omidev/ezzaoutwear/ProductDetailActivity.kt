package com.omidev.ezzaoutwear


import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.omidev.ezzaoutwear.utils.Utils
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.toolbar.*
import java.net.URLEncoder


class ProductDetailActivity : AppCompatActivity(), View.OnClickListener {
    var name = ""
    var photo = 0
    var desc = ""
    var stock = 0
    var price = 0
    var categories = ""
    companion object{
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_PHOTO = "extra_photo"
        const val EXTRA_DESCRIPTION = "extra_desc"
        const val EXTRA_STOCK = "extra_stock"
        const val EXTRA_PRICE = "extra_price"
        const val EXTRA_CATEGORIES = "extra_categories"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        initComponent()
    }

    fun initComponent() {
        setSupportActionBar(toolbar)
        lblToolbar.text = title
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        toolbar.navigationIcon = null
        lblToolbar.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        ivleft.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.nav_back))
        ivleft.setOnClickListener(this)
        btn_tanya_penjual.setOnClickListener(this)
        ivAbout.visibility= View.GONE
        name = intent.getStringExtra(EXTRA_NAME)
        photo = intent.getIntExtra(EXTRA_PHOTO,0)
        desc = intent.getStringExtra(EXTRA_DESCRIPTION)
        stock = intent.getIntExtra(EXTRA_STOCK,0)
        price = intent.getIntExtra(EXTRA_PRICE,0)
        categories = intent.getStringExtra(EXTRA_CATEGORIES)
        Glide.with(this)
            .load(photo)
            .apply(RequestOptions().override(350,550))
            .into(ivDetail)

        tv_nama_produk.text = name
        tv_label_price.text = Utils.getShownNominalNoneConvert(price.toString())
        tv_value_stock.text = stock.toString()+" buah"
        tv_description.text = desc
    }

    override fun onClick(v: View?) {
       when(v){
           ivleft->{
               finish()
           }
           btn_tanya_penjual->{
               openWhatsApp("6285155249936","Halo ka admin, apakah produk $categories $name masih tersedia?")
           }
       }
    }

    private fun openWhatsApp(number: String, message: String) {
        try {
            val packageManager: PackageManager = this.getPackageManager()
            val i = Intent(Intent.ACTION_VIEW)
            val url =
                "https://api.whatsapp.com/send?phone=$number&text=" + URLEncoder.encode(
                    message,
                    "UTF-8"
                )
            i.setPackage("com.whatsapp")
            i.data = Uri.parse(url)
            if (i.resolveActivity(packageManager) != null) {
                startActivity(i)
            } else {
              Toast.makeText(this,"Whatsapp app not installed in your phone",Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            Log.e("ERROR WHATSAPP", e.toString())
            Toast.makeText(this,"Whatsapp app not installed in your phone",Toast.LENGTH_LONG).show()
        }
    }
}
